/**
 *  Date created: 9/14
 *  creator: John Yun
 *  Class: CS 49J
 */

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        int iFirst, iSecond;

        Scanner read = new Scanner(System.in);
        System.out.print("Please enter the first time: ");
        iFirst = read.nextInt();
        System.out.print("Please enter the second time: ");
        iSecond= read.nextInt();

        int iFirMin, iSecMin, iFirHr, iSecHr, iResultHr, iResultMin;
        iFirHr = iFirst/100;
        iSecHr = iSecond/100;
        iFirMin = iFirst%100;
        iSecMin = iSecond%100;

        //check if correct times are inputted
        //System.out.println(iFirHr + ":" + iFirMin );

        if(iFirst > iSecond){

            iResultHr = 23 - iFirHr;
            iResultMin = 60 - iFirMin;

            iResultMin += iSecMin;
            iResultHr += (iSecHr + iResultMin/60);
            if(iResultMin > 60) iResultMin /= 60;
            //System.out.println(iResultHr + ":" + iResultMin);
        }
        else {
            iResultHr = iSecHr - iFirHr;
            iResultMin = iSecMin - iFirMin;
            if(iResultMin < 0){
                iResultHr--;
                iResultMin += 60;
            }

            //System.out.println(iResultHr + ":" + iResultMin);
        }
        System.out.printf("%d hours %d minutes", iResultHr, iResultMin);

    }
}

    /************************************************************************
     *     SAMPLE OUTPUT
     *
     *     Please enter the first time: 1840
     *     Please enter the second time: 1320
     *     18 hours 40 minutes
     *     Process finished with exit code 0
     */
